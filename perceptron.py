import processor
import sys
import random

def calc_Sum(weight_Dict, input_Vector, words, bias):
	signal_Sum = bias
	for index in range(len(input_Vector)):
		input_Signal = input_Vector[index]
		word = words[index]
		if word not in weight_Dict:
			weight_Dict[word] = random.uniform(-1,1)
		signal_Sum += input_Signal * weight_Dict[word]
	return signal_Sum 

def activate(signal_Sum):
	if signal_Sum < 0:
		return -1
	else:
		return 1


def delta_Weight(old_Weights, new_Weights):
	avg_WeightsOld = sum(old_Weights)/len(old_Weights)
	avg_WeightsNew = sum(new_Weights)/len(new_Weights)
	return abs(abs(avg_WeightsOld) - abs(avg_WeightsNew))

def adjustWeights(learning_Rate, error, weight_Dict, input_Vector, words):
	old_Weights = []
	new_Weights = []
	for index in range(len(input_Vector)):
		input_Signal = input_Vector[index]
		word = words[index]
		old_Weights.append(weight_Dict[word])
		new_Weight = weight_Dict[word] + error * learning_Rate * input_Signal
		weight_Dict[word] = new_Weight
		new_Weights.append(weight_Dict[word])
	weight_Diff = delta_Weight(old_Weights, new_Weights)
	#if weight_Diff < learning_Rate:
	#	return True
	return False


def perceptron(expected, bias, weight_Dict, input_Vector, train_File):
	learning_Rate = 0.006
	words = processor.get_Words(train_File, sys.argv)
	signal_Sum = calc_Sum(weight_Dict, input_Vector, words, bias)
	error = expected - activate(signal_Sum) 
	stop_Flag = False
	if error != 0: 
		bias = bias + error * learning_Rate
		stop_Flag = adjustWeights(learning_Rate, error, weight_Dict, input_Vector, words)
	return bias, stop_Flag

def main():
	accuracies = []
	precisions = []
	recalls = []
	pos_Files, neg_Files, vectors = processor.process(sys.argv)
	training_Files = []
	for index in range(len(pos_Files)):
		training_Files.append(pos_Files[index])
		training_Files.append(neg_Files[index])
	training_Subsets = processor.split_List(training_Files, 5)
	vector_Subsets = processor.split_List(vectors, 5)
	for index in range(5):
		weight_Dict = {}
		bias = 0
		print("\n--- Test " + str(index + 1) + " ---")
		train_Files = [elem for x in [x for ind, x in enumerate(training_Subsets) if ind != index] for elem in x]
		train_Vectors = [elem for x in [x for ind, x in enumerate(vector_Subsets) if ind != index] for elem in x]
		test_Files = training_Subsets[index]
		test_Vectors = vector_Subsets[index]
		for index in range(len(train_Vectors)):
			if index%2 == 0:
				bias, stop_Flag = perceptron(1, bias, weight_Dict, train_Vectors[index], train_Files[index])
				if stop_Flag:
					break
			else:
				bias, stop_Flag = perceptron(-1, bias, weight_Dict, train_Vectors[index], train_Files[index])
				if stop_Flag:
					break

		true_Pos = 0
		false_Pos = 0
		true_Neg = 0
		false_Neg = 0

		for index in range(len(test_Vectors)):
			words = processor.get_Words(test_Files[index], sys.argv)
			if index%2 == 0:
				if activate(calc_Sum(weight_Dict, test_Vectors[index], words, bias)) == 1:
					true_Pos += 1
				else:
					false_Neg +=1
			else:
				if activate(calc_Sum(weight_Dict, test_Vectors[index], words, bias)) == -1:
					true_Neg += 1
				else:
					false_Pos += 1

		accuracy, precision, recall = processor.calc_Performance(true_Pos, false_Pos, true_Neg, false_Neg)
		print(processor.perf_toString(accuracy, precision, recall))
		accuracies.append(accuracy)
		precisions.append(precision)
		recalls.append(recall)

	
	print(processor.feature_toString(sys.argv))
	print(processor.allPerf_toString(accuracies, precisions, recalls))

main()
