import processor
import sys
import math
import string


def update_Counts(training_File, class_Dict):
	input_Type, punct_Flag = processor.get_ProcessOpt(sys.argv)
	feature_Data = open(training_File).read()
	if not punct_Flag:
		feature_Data = feature_Data.translate(None, string.punctuation)
	features = feature_Data.split()
	if input_Type == "frequency":
		for feature in features:
			if feature in class_Dict:
				class_Dict[feature] += 1
			else:
				class_Dict[feature] = 1
	else:
		words_Seen = []
		for feature in features:
			if feature not in words_Seen:
				if feature in class_Dict:
					class_Dict[feature] += 1
				else:
					class_Dict[feature] = 1

def calc_Naive(test_Data, class_Dict, other_Dict, total_Class, total_Other):
	total_Vocab = len(class_Dict)
	for word in other_Dict:
		if word not in class_Dict:
			total_Vocab += 1
	test_Features = open(test_Data).read().split()
	prob_total = 0
	prob_Class = float(total_Class)/(total_Class + total_Other)
	total_words = sum(class_Dict.values())
	for feature in test_Features:
		feature_Count = 0
		if feature in class_Dict:
			feature_Count = class_Dict[feature]
		prob_total += math.log((float(feature_Count + 1)/(total_words + total_Vocab)),2)
	return prob_total * prob_Class


def main():
	accuracies = []
	precisions = []
	recalls = []
	pos_Files, neg_Files, vectors = processor.process(None)
	pos_Subsets = processor.split_List(pos_Files, 5)
	neg_Subsets = processor.split_List(neg_Files, 5)
	for index in range(5):
		pos_Dict = {}
		neg_Dict = {}
		total_Pos = 0
		total_Neg = 0
		print("\n--- Test " + str(index + 1) + " ---")
		pos_TrainingSet = [elem for x in [x for ind, x in enumerate(pos_Subsets) if ind != index] for elem in x]
		neg_TrainingSet = [elem for x in [x for ind, x in enumerate(neg_Subsets) if ind != index] for elem in x]
		pos_TestSet = pos_Subsets[index]
		neg_TestSet = neg_Subsets[index]

		for index in range(len(pos_TrainingSet)):
			update_Counts(pos_TrainingSet[index], pos_Dict)
			total_Pos += 1
			update_Counts(neg_TrainingSet[index], neg_Dict)
			total_Neg += 1

		true_Pos = 0
		false_Pos = 0
		true_Neg = 0
		false_Neg = 0

		for index in range(len(pos_TestSet)):
			pos_Prob = calc_Naive(pos_TestSet[index], pos_Dict, neg_Dict, total_Pos, total_Neg)
			neg_Prob = calc_Naive(pos_TestSet[index], neg_Dict, pos_Dict, total_Neg, total_Pos)
			
			if pos_Prob > neg_Prob:
				true_Pos += 1
			else:
				false_Neg += 1 
			pos_Prob = calc_Naive(neg_TestSet[index], pos_Dict, neg_Dict, total_Pos, total_Neg)
			neg_Prob = calc_Naive(neg_TestSet[index], neg_Dict, pos_Dict, total_Neg, total_Pos)
			if neg_Prob > pos_Prob:
				true_Neg += 1
			else:
				false_Pos += 1 

		accuracy, precision, recall = processor.calc_Performance(true_Pos, false_Pos, true_Neg, false_Neg)
		print(processor.perf_toString(accuracy, precision, recall))
		accuracies.append(accuracy)
		precisions.append(precision)
		recalls.append(recall)

	
	print(processor.feature_toString(sys.argv))
	print(processor.allPerf_toString(accuracies, precisions, recalls))

main()
