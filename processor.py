import sys
import os
import string
import random
import math

pos_Dir = "review_polarity/txt_sentoken/pos"
neg_Dir = "review_polarity/txt_sentoken/neg"

def get_ProcessOpt(args):
	input_Type = "frequency"
	punct_Flag = True
	if not args:
		return input_Type, punct_Flag
	else:
		for index in range(len(args)):
			arg = args[index].lower()
			if arg in ["--frequency", "--binary"]:
				input_Type = arg[2:]
			elif arg in ["--nopunct", "--punct"]:
				if arg == "--nopunct":
					punct_Flag = False
	return input_Type, punct_Flag

def split_List(list, multiple):
	new_List = []
	length = len(list)/multiple
	for index in range(multiple):
		if index < multiple - 1:
			new_List.append(list[(index*length):(index+1)*length])
		else:
			new_List.append(list[(index*length):])
	return new_List

def get_TrainingFiles(pos_Dir, neg_Dir):
	pos_Files = []
	neg_Files = []
	pos_DirFiles = os.listdir(pos_Dir)
	neg_DirFiles = os.listdir(neg_Dir)
	for index in range(len(pos_DirFiles)):
		pos_Files.append(pos_Dir + "/" + pos_DirFiles[index])
		neg_Files.append(neg_Dir + "/" + neg_DirFiles[index])
	random.shuffle(pos_Files)
	random.shuffle(neg_Files)
	return pos_Files, neg_Files

def get_Freq(words):
	freq_Dict = {}
	for word in words:
		if word in freq_Dict:
			freq_Dict[word] += 1
		else:
			freq_Dict[word] = 1
	return freq_Dict

def create_Vector(training_File, input_Type, punct_Flag):
	input_Vector = []
	training_Data = open(training_File).read()
	if not punct_Flag:
		training_Data = training_Data.translate(None, string.punctuation)
	training_Words = training_Data.split()
	if input_Type == "frequency":
		freq_Dict = get_Freq(training_Words)
		for word in training_Words:
			input_Vector.append(freq_Dict[word])
	else:
		for word in training_Words:
			input_Vector.append(1)
	return input_Vector

def calc_Sum(weight_Dict, input_Vector, bias):
	signal_Sum = bias
	for word in input_Vector:
		input_Signal = input_Vector[word]
		if word not in weight_Dict:
			weight_Dict[word] = random.uniform(-1,1)
		signal_Sum += input_Signal * weight_Dict[word]
	return signal_Sum 

def activate(signal_Sum):
	if signal_Sum < 0:
		return -1
	else:
		return 1

def adjustWeights(error, weight_Dict, input_Vector, learning_Rate):
	for word in input_Vector:
		input_Signal = input_Vector[word]
		new_Weight = weight_Dict[word] + error * learning_Rate * input_Signal
		weight_Dict[word] = new_Weight

def perceptron(expected, weight_Dict, input_Vector, bias):
	learning_Rate = .01
	signal_Sum = calc_Sum(weight_Dict, input_Vector, bias)
	error = expected - activate(signal_Sum) 
	bias = bias + error * learning_Rate
	adjustWeights(error, weight_Dict, input_Vector, learning_Rate)
	return bias

def calc_Acc(true_Pos, false_Pos, true_Neg, false_Neg):
	return (float(true_Pos + true_Neg)/(true_Pos + true_Neg + false_Pos + false_Neg))

def calc_Perc(true_Pos, false_Pos, true_Neg, false_Neg):
	try:
		pos_Perc = float(true_Pos)/(true_Pos + false_Pos)
		neg_Perc = float(true_Neg)/(true_Neg + false_Neg)
		return (float(pos_Perc + neg_Perc)/2)
	except:
		return 0

def calc_Recall(true_Pos, false_Pos, true_Neg, false_Neg):
	try:	
		pos_Recall = float(true_Pos)/(true_Pos + false_Neg)
		neg_Recall = float(false_Pos)/(false_Pos + true_Neg)
		return (float(pos_Recall + neg_Recall)/2)	
	except:
		return 0

def get_Words(file, args):
	input_Type, punct_Flag = get_ProcessOpt(args)
	file_Data = open(file).read()
	if not punct_Flag:
		file_Data = file_Data.translate(None, string.punctuation)
	return file_Data.split()

def process(args):
	input_Type, punct_Flag = get_ProcessOpt(args)
	pos_Files, neg_Files = get_TrainingFiles(pos_Dir, neg_Dir)
	vectors = []
	for index in range(len(pos_Files)):
		vectors.append(create_Vector(pos_Files[index], input_Type, punct_Flag))
		vectors.append(create_Vector(neg_Files[index], input_Type, punct_Flag))
	return pos_Files, neg_Files, vectors

def calc_Performance(true_Pos, false_Pos, true_Neg, false_Neg):
	accuracy = calc_Acc(true_Pos, false_Pos, true_Neg, false_Neg)
	precision = calc_Perc(true_Pos, false_Pos, true_Neg, false_Neg)
	recall = calc_Recall(true_Pos, false_Pos, true_Neg, false_Neg)
	return accuracy, precision, recall

def perf_toString(accuracy, precision, recall):
	return "Accuracy: " + str(accuracy) + "\nPrecision: " + str(precision) + "\nRecall: " + str(recall)

def find_MaxMin(performances):
	max_Index = performances.index(max(performances)) + 1
	max_Value = round(max(performances),3)
	min_Index = performances.index(min(performances)) + 1
	min_Value = round(min(performances),3)
	return max_Index, max_Value, min_Index, min_Value

def avg_toString(performances, perf_Type):
	avg_Perfomance = (sum(performances))/len(performances)
	return "Average " + perf_Type + ": " + str(avg_Perfomance) + "\n"

def maxMin_toString(performances, perf_Type):
	max_Index, max_Value, min_Index, min_Value = find_MaxMin(performances)
	return "Max " + perf_Type + " at fold " + str(max_Index) + ": " + str(max_Value) + "\tMin " + perf_Type + " at fold " + str(min_Index) + ": " + str(min_Value) + "\n"

def allPerf_toString(accuracies, precisions, recalls):
	header = "--- Overall Performance ---\n"
	acc_String = avg_toString(accuracies, "Accuracy")
	prec_String = avg_toString(precisions, "Precision")
	rec_String = avg_toString(recalls, "Recall")
	mmAcc_String = maxMin_toString(accuracies, "Accuracy")
	mmPrec_String = maxMin_toString(precisions, "Precision")
	mmRec_String = maxMin_toString(recalls, "Recall")
	return header + acc_String + prec_String + rec_String + mmAcc_String + mmPrec_String + mmRec_String

def feature_toString(args):
	input_type, punct = get_ProcessOpt(args)
	return "\nVECTOR TYPE: " + input_type.upper() + "\nPUNCTUATION: " + str(punct).upper() + "\n"


